import requests
import os, logging


def send_notification(
    topic: str,
    data: str,
    title: str,
    tags: str,
    priority="high",
    instance="https://ntfy.sh",
):
    headers = {}
    if title is not None:
        headers["Title"] = title
    if priority is not None:
        headers["Priority"] = priority
    if tags is not None:
        headers["Tags"] = tags
    requests.post(
        url=instance + "/" + topic,
        timeout=1,
        data=data.encode("utf-8"),
        allow_redirects=True,
        headers=headers,
    )
    logging.debug("Отправлено уведомление через ntfy.")


def send_env_notification(data: str):
    send_notification(
        data=data,
        topic=os.getenv("NTFY_TOPIC"),
        instance=os.getenv("NTFY_INSTANCE", "https://ntfy.sh"),
        priority=os.getenv("NTFY_PRIORITY"),
        title=os.getenv("NTFY_TITLE"),
        tags=os.getenv("NTFY_TAGS"),
    )
