"""
Пожалуйста, прочитайте README.md перед проверкой!
"""

from host import Host
import itertools, argparse, ctypes, csv, time, copy
import utils, socket, coloredlogs, logging, os, ntfy
from multiprocessing import dummy as mp

parser = argparse.ArgumentParser(description="Приложение для мониторинга серверов.")

parser.add_argument(
    "--input",
    type=str,
    help="Указывает файл с данными. По умолчанию hosts.csv.",
    default="hosts.csv",
)

args = parser.parse_args()

hosts: list[Host] = []
_previous_results: enumerate = []

coloredlogs.install(level="DEBUG")

logging.info("А вы прочитали README.md? ;)")

csvfile = open(args.input, newline="", encoding="utf-8")
reader = csv.reader(csvfile, delimiter=";")

try:
    can_use_icmp = os.geteuid() == 0
except AttributeError:
    can_use_icmp = ctypes.windll.shell32.IsUserAnAdmin() != 0

for row in reader:
    if row[0] == "Host" and row[1] == "Ports" and len(row) == 2:
        continue
    if row[0] == "":
        logging.critical("Имя хоста не может быть пустой строкой. Завершение работы.")
        exit(127)
    try:
        ports = [int(p) for p in row[1:] if p != ""]
    except ValueError:
        logging.critical(
            "Порт не является целым числом при обработке '%s'! Завершение работы.",
            row[0],
        )
        exit(127)
    if len(ports) == 0 and not can_use_icmp:
        logging.warning("Вы пытаетесь выполнить ICMP проверку без root-доступа!")
    if utils.is_ip_address(row[0]):
        hosts.append(Host(ip=row[0], ports=ports, parent_hostname=None))
    else:
        try:
            hosts.extend(
                [Host(ip, ports, row[0]) for ip in utils.resolve_domain(row[0])]
            )
        except socket.gaierror as err:
            logging.warning(
                "Ошибка резолвинга '%s'! %s", row[0], utils.clean_sock_error(err)
            )

csvfile.close()

while True:
    start = time.time()
    pool = mp.Pool(processes=min(len(hosts), int(os.getenv("HOST_THREADS", "999999"))))
    results = pool.map(lambda h: h.check(), hosts)
    logging.debug("Проверка закончена.")
    print()
    flattened = itertools.chain.from_iterable(results)
    tmp2 = copy.deepcopy(list(flattened))
    enumerated = enumerate(tmp2)

    diff_up = []
    diff_down = []
    tmp = copy.deepcopy(list(_previous_results))
    if len(list(tmp)) != 0:
        tmp3 = copy.copy(list(enumerated))
        diff_down = [res for i, res in tmp3 if not res[0] and tmp[i][0]]
        diff_up = [res for i, res in tmp3 if res[0] and not tmp[i][0]]

    _previous_results = copy.deepcopy(tmp2)
    to_print: str = ""

    if len(diff_down) != 0:
        to_print += "Следующие сервисы упали со времени последней проверки\n"
        table = utils.generate_table(diff_down)
        to_print += str(table) + "\n\n"

    if len(diff_up) != 0:
        to_print += "Следующие сервисы встали со времени последней проверки:\n"
        table = utils.generate_table(diff_up)
        to_print += str(table) + "\n\n"

    table = utils.generate_table(tmp2)
    to_print += "Статус всех сервисов:\n"
    to_print += str(table)

    print(to_print)

    ###### Ntfy
    if os.getenv("NTFY_TOPIC") is not None and (
        len(diff_up) != 0 or len(diff_down) != 0
    ):
        try:
            ntfy.send_env_notification(to_print)
        except Exception as err:
            logging.error("Ntfy got an error: %s", str(err))
    ###### Конец Ntfy

    if time.time() > start + int(os.getenv("INTERVAL", "60")):
        logging.warning(
            "Проверка заняла %d секунд, дольше разрешённых %s-и секунд!",
            time.time() - start,
            os.getenv("INTERVAL", "60"),
        )
    time.sleep((start + int(os.getenv("INTERVAL", "60"))) - time.time())
