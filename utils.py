from prettytable import PrettyTable
import re, os, time, socket, ipaddress

socket_error_regex = re.compile(r"[Errno [\-0-9]+\] (.+)")

hide_err_regex = re.compile(
    r'(Address family for hostname not supported)|(Cannot resolve address ".+".+)'
)


def clean_sock_error(err: socket.gaierror) -> str:
    if not isinstance(err, socket.error):
        return str(err)
    return socket_error_regex.search(str(err)).group(1)


def resolve_domain(domain: str) -> list[str]:
    return list(set([el[4][0] for el in socket.getaddrinfo(domain, 80)]))


def check_tcp_port(ip: str, port: int) -> tuple[bool, str, int, int]:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(int(os.getenv("ICMP_TIMEOUT", "2")))
    try:
        try:
            start = time.time()
            s.connect((ip, port))
            return True, "", port, (time.time() - start) * 1000
        except TimeoutError as err:
            return False, str(err), port, -1
    except socket.error as err:
        return False, clean_sock_error(err), port, -1


def is_ip_address(value: str) -> bool:
    try:
        ipaddress.ip_address(value)
        return True
    except ValueError:
        return False


def generate_table(data, title: str = None):
    table = PrettyTable()
    if title is not None:
        table.title = title
    table.field_names = ["Адрес", "Тип", "Статус", "RTT (мс)", "Порт", "Статус порта"]
    for check in data:
        table.align = "l"
        if check[2] == -2 and any(
            f[1] == "DNS" and f[0] == check[4].parent_hostname for f in table.rows
        ):
            continue
        table.add_row(
            [
                check[4].parent_hostname
                if check[2] == -2
                else f"{check[4].ip} ({check[4].parent_hostname})"
                if check[4].parent_hostname is not None
                else check[4].ip,
                "ICMP" if check[2] == -1 else "DNS" if check[2] == -2 else "TCP",
                "Работает" if check[0] else "Не работает",
                "N/A" if check[3] < 0 else f"{round(check[3], 2)} мс",
                check[2] if check[2] >= 0 else "N/A",
                "N/A" if check[2] <= 0 else "Открыт" if check[0] else "Неизвестно",
            ]
        )
    return table
