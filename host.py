import utils
import os, socket, logging
from pythonping import ping
from multiprocessing import dummy as mp

_printed_ipv6_warning: bool = False


class Host:
    ip: str = None
    ports: list[int] = None
    parent_hostname: str = None

    def __init__(self, ip: str, ports: list[int], parent_hostname: str):
        self.ip = ip
        self.ports = ports
        self.parent_hostname = parent_hostname

    def check(self):
        # Без этого сильно сложнее
        global _printed_ipv6_warning
        checks: list[tuple[bool, str, int, Host]] = []
        if self.parent_hostname is not None:
            try:
                # Тут можно оптимизировать, но не нужно, так как ОС кэширует DNS запросы
                utils.resolve_domain(self.parent_hostname)
                checks.append((True, "", -2, -1, self))
            except socket.gaierror as err:
                checks.append((False, utils.clean_sock_error(err), -1, -1, self))

        if len(self.ports) == 0:
            try:
                result = ping(
                    self.ip, timeout=int(os.getenv("ICMP_TIMEOUT", "2")), count=3
                )
                checks.append((True, "", -1, result.rtt_avg * 1000, self))
            except RuntimeError as err:
                if utils.hide_err_regex.match(str(err)) and not bool(
                    os.getenv("SHOW_IPV6_ERRORS", "")
                ):
                    if not _printed_ipv6_warning:
                        logging.warning(
                            "Часть ошибок была проигнорирована из-за отсутствия поддержки IPv6."
                        )
                    _printed_ipv6_warning = True
                else:
                    checks.append(
                        (False, "Ошибка ICMP ping: " + str(err), -1, -1, self)
                    )
        else:
            pool = mp.Pool(processes=len(self.ports))
            results = pool.map(
                lambda p: (*utils.check_tcp_port(self.ip, p), self), self.ports
            )

            filtered = list(
                filter(
                    lambda r: not utils.hide_err_regex.match(r[1])
                    and not bool(os.getenv("SHOW_IPV6_ERRORS", "")),
                    results,
                )
            )

            if len(filtered) < len(results) and not _printed_ipv6_warning:
                logging.warning(
                    "Часть ошибок была проигнорирована из-за отсутствия поддержки IPv6."
                )
                _printed_ipv6_warning = True

            checks.extend(filtered)
        return checks
