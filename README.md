## Игнорируемые ошибки

Следующие ошибки по умолчанию игнорируются (так как они вызваны отсутствием поддержки IPv6) и могут быть включены опцией **`--show-ipv6-errors`**:

- Address family for hostname not supported
- Cannot resolve address "\[...]", try verify your DNS or host file

## Опции

**`--input`** — Входной файл с данными. По умолчанию hosts.csv.

## Переменные окружения

**`LOG_LEVEL`** — Смысл понятен. Уровень логирования. По умолчанию `DEBUG`.

**`SHOW_IPV6_ERRORS`** — Включает ошибки IPv6, см. выше.

**`INTERVAL`** — Интервал проверки в секундах. По умолчанию 60.

**`ICMP_TIMEOUT`** — Таймаут для ICMP пингов в секундах. По умолчанию 2.

**`TCP_TIMEOUT`** — Таймаут для проверки TCP портов в секундах. По умолчанию 2.

**`HOST_THREADS`** — Максимальное количество потоков, использующихся при проверке хостов. По умолчанию число хостов для проверки.

### Ntfy

**`NTFY_TOPIC`** — Тема для провайдера уведомлений ntfy. См. [документацию](https://ntfy.sh). Включает ntfy.

**`NTFY_INSTANCE`** — Инстанс ntfy. По умолчанию: `https://ntfy.sh`.

**`NTFY_PRIORITY`** — Приоритет уведомления для ntfy. По умолчанию: `high`.

**`NTFY_TITLE`** — Название уведомления для ntfy.

**`NTFY_TAGS`** — Эмоджи-теги уведомления для ntfy.
