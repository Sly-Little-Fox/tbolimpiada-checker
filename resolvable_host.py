""" import re
import socket
from host import Host

errorRegex = re.compile(r"[Errno [\-0-9]+\] (.+)")

class ResolvableHost:
    hostname: str = None
    ports: list[int] = None
    subhosts: list[Host] = []

    def __init__(self, hostname: str, ports: set) -> None:
        self.hostname = hostname
        self.ports = ports

    def check(self) -> list(tuple[bool, str]): # up, error
        ips = []
        try:
            ips = self.resolve()
        except socket.gaierror as err:
            return [(False, errorRegex.search(str(err)).group(1))]
        return [Host(ip, self.ports, ``).check for ip in ips]
 """
